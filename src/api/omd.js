export default class ODM {
  static async searchByTitle(title) {
    const response = await fetch(
      `http://www.omdbapi.com/?apikey=7be29a81&t=${title}`
    )

    return await response.json()
  }
}
