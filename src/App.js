import React from 'react'
import { useSelector } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'

import SearchView from './views/search'
import SearchResultsView from './views/search-results'
import MovieDetailsView from './views/movie-details'

import './App.css'

function App() {
  const state = useSelector((state) => state)

  return (
    <div className="cinema-app">
      <Grid container justify="center" alignContent="center">
        <h1 className="cinema-app-header">Search for a Movie</h1>
        <Router>
          <Route
            render={(props) => (
              <Switch>
                <Route
                  exact
                  path="/"
                  component={() => <SearchView {...props} />}
                />
                <Route
                  exact
                  path="/search-results"
                  component={() => <SearchResultsView {...props} />}
                />
                <Route
                  path="/movie-details/:movieID"
                  component={() => <MovieDetailsView {...props} />}
                />
              </Switch>
            )}
          />
        </Router>
        {state.movieResults.searching && (
          <div className="cinema-app-searching">
            <CircularProgress disableShrink />
          </div>
        )}
      </Grid>
    </div>
  )
}

export default App
