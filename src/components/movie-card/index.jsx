import React from 'react'
import { Link } from 'react-router-dom'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import useStyles from './styles'

export default function MovieCard({ results }) {
  const classes = useStyles()

  return (
    <React.Fragment>
      {results.map((movie) => (
        <Card className={classes.root}>
          <CardActionArea>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {movie.Title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {movie.Plot}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Link to={`movie-details/${movie.imdbID}`}>
              <Button size="small" color="primary">
                More
              </Button>
            </Link>
          </CardActions>
        </Card>
      ))}
    </React.Fragment>
  )
}
