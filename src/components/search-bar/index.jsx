import React from 'react';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import useStyles from './styles'

export default function SearchBar({ onSearch, search, setSearch }) {
  const classes = useStyles();

  return (
    <Paper component="form" className={classes.root}>
      <form className={classes.root} onSubmit={onSearch}>
        <InputBase
          className={classes.input}
          placeholder="Search movie by title and hit enter"
          inputProps={{ 'aria-label': 'search movie by title' }}
          value={search}
          onChange={e => setSearch(e.target.value)}
        />
        <IconButton type="submit" className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </form>
    </Paper>
  );
}
