import React from 'react'
import { Redirect } from 'react-router-dom'

import useStyles from './styles'

export default function MovieDetails({ movie }) {
	const classes = useStyles()
	let hasRottenTomatoRating = false
	if (movie) {
		[hasRottenTomatoRating] = movie.Ratings.filter(
			(item) => item.Source === 'Rotten Tomatoes'
		)
	}
	
  return (
    <React.Fragment>
      {movie ? (
        <div className={classes.root}>
          <img src={movie.Poster} alt={movie.Title} />
					<div className={classes.content}>
						<p>Year: {movie.Year}</p>
						<p>Relaese Date: {movie.Released}</p>
						<p>Genre: {movie.Genre}</p>
						<p>Duration: {movie.Runtime}</p>
						<p>Rotten Tomato Rating: {hasRottenTomatoRating && hasRottenTomatoRating.Value}</p>
					</div>
        </div>
      ) : (
        <Redirect to="/" />
      )}
    </React.Fragment>
  )
}
