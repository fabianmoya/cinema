import {
  SET_RESULTS,
  SET_RESULTS_NOT_FOUND,
  SET_RESULTS_START,
  SET_RESULTS_END,
} from '../types'

const initialState = {
  results: [],
  found: false,
  searching: false,
}

export default function resultsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_RESULTS_START:
      return {
        ...state,
        searching: true,
      }

    case SET_RESULTS_END:
      return {
        ...state,
        searching: false,
      }

    case SET_RESULTS_NOT_FOUND:
      return {
        ...state,
        results: [],
        found: false,
      }

    case SET_RESULTS:
      return {
        ...state,
        results: [action.payload],
        found: true,
      }
    default:
      return state
  }
}
