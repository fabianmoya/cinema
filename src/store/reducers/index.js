import { combineReducers } from 'redux'
import resultsReducer from './results-reducer'

export default combineReducers({
  movieResults: resultsReducer,
})