import {
  SET_RESULTS,
  SET_RESULTS_NOT_FOUND,
  SET_RESULTS_START,
  SET_RESULTS_END,
} from '../types'
import OMD from '../../api/omd'

export const actionSetResults = (search) => async (dispatch) => {
  dispatch({ type: SET_RESULTS_START })

  const movies = await OMD.searchByTitle(search)

  if (movies.Response === 'False') {
    dispatch({
      type: SET_RESULTS_NOT_FOUND,
    })
  } else {
    dispatch({
      type: SET_RESULTS,
      payload: movies,
    })
  }

  dispatch({ type: SET_RESULTS_END })
}
