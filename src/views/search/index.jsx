import React from 'react'
import Grid from '@material-ui/core/Grid';

import SearchBar from '../../components/search-bar'

import useController from './index.controller'

export default function SearchView({ history }) {
  const { search, setSearch, onSearch } = useController({ history })

  return (
    <Grid item xs={12} md={12} justify="center">
      <SearchBar search={search} setSearch={setSearch} onSearch={onSearch} />
    </Grid>
  )
}
