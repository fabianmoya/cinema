import React from 'react'
import { useDispatch } from 'react-redux'

import { actionSetResults } from '../../store/actions/results-actions'

export default function useController({ history }) {
  const [search, setSearch] = React.useState('')
  const dispatch = useDispatch()

  const onSearch = e => {
    e.preventDefault()
    dispatch(actionSetResults(search))
    history.push('/search-results')
  }

  return {
    search,
    setSearch,
    onSearch,
  }
}
