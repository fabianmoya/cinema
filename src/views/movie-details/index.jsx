import React from 'react'
import Grid from '@material-ui/core/Grid'
import { Link } from 'react-router-dom'

import MovieDetails from '../../components/movie-details'

import useController from './index.controller'

export default function MovieDetailView() {
  const { movie } = useController()

  return (
    <Grid item xs={12} md={12} justify="center">
      <Link style={{ color: '#FFF' }} to="/">
        Back to search
      </Link>
      <MovieDetails movie={movie} />
    </Grid>
  )
}
