import { useSelector } from "react-redux"
import { useParams } from "react-router-dom"

export default function useController() {
  const { movieID } = useParams()
  const state = useSelector(state => state)
  const [movie] = state.movieResults.results.filter(item => item.imdbID === movieID)

  return {
    movie
  }
}
