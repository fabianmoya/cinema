import React from 'react'
import Grid from '@material-ui/core/Grid'
import { Link } from 'react-router-dom'

import MovieCard from '../../components/movie-card'

import useController from './index.controller'

export default function SearchResultsView() {
  const { movieResults } = useController()

  return (
    <Grid item xs={12} md={12} justify="center">
      {movieResults.found ? (
        <React.Fragment>
          <Link style={{ color: '#FFF' }} to='/'>Back to search</Link>
          <h1 style={{ color: '#FFF' }}>Results:</h1>
          <MovieCard results={movieResults.results}/>
        </React.Fragment>
      ) : (
        <h1 style={{ color: '#FFF' }}>Not Matching Results</h1>
      )}
    </Grid>
  )
}
