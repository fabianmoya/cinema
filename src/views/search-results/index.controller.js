import { useSelector } from 'react-redux'

export default function useController() {
  const state = useSelector(state => state)

  return {
    movieResults: state.movieResults
  }
}
